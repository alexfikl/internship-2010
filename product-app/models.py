# -*- coding: utf-8 -*-
from flaskext.sqlalchemy import SQLAlchemy
from productapp import app

db = SQLAlchemy(app)

class ProductType(db.Model):
    __tablename__ = 'producttype'

    id = db.Column(db.Integer, primary_key=True)
    sName = db.Column(db.String)
    sDescription = db.Column(db.String)

    product = db.relationship('Product')

    def __init__(self, name, desc):
        self.sName = name
        self.sDescription = desc

    def __repr__(self):
        return '<ProductType({sName}, {sDescription})>'.format(self.serialize())

    def serialize(self):
        return {
            'id': self.id,
            'sName': self.sName,
            'sDescription': self.sDescription
            }

class ProductUM(db.Model):
    __tablename__ = 'productum'

    id = db.Column(db.Integer, primary_key=True)
    sName = db.Column(db.String)
    sShort = db.Column(db.String)

    product = db.relationship('Product')

    def __init__(self, name, short):
        self.sName = name
        self.sShort = short

    def __repr__(self):
        return '<ProductUM({sName}, {sShort})>'.format(self.serialize())

    def serialize(self):
        return {
            'id': self.id,
            'sName': self.sName,
            'sShort': self.sShort
            }

class Company(db.Model):
    __tablename__ = 'company'

    id = db.Column(db.Integer, primary_key=True)
    sType = db.Column(db.String)
    sName = db.Column(db.String)
    sLocation = db.Column(db.String)
    sCUI = db.Column(db.String)

    product = db.relationship('Product')

    def __init__(self, ctype, name, reg, cui):
        return self.sCompanyName

    def __repr__(self):
        return '<Company({sName} {sType}, {sLocation}, {sCUI})>'.format(self.serialize())

    def serialize(self):
        return {
            'id': self.id,
            'sType': self.sType,
            'sName': self.sName,
            'sLocation': self.sLocation,
            'sCUI': self.sCUI
            }

class Product(db.Model):
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key=True)
    sName = db.Column(db.String)
    nPrice = db.Column(db.Float)
    nAmount = db.Column(db.Integer)
    fType = db.Column(db.Integer, db.ForeignKey('producttype.id'))
    fUM = db.Column(db.Integer, db.ForeignKey('productum.id'))
    fCompany = db.Column(db.Integer, db.ForeignKey('company.id'))

    def __init__(self, name, price, amount, tid, umid, cid):
        self.sName = name
        self.nPrice = price
        self.nAmount = amount
        self.fType = tid
        self.fUM = umid
        self.fCompany = cid

    def __repr__(self):
        return "<Product({sName}, {nPrice}, {nAmount}, {fType}, {fUM}, {fCompany})>".format(self.serialize())

    def serialize(self):
        return {
            'id': self.id,
            'sName': self.sName,
            'nPrice': self.nPrice,
            'nAmount': self.nAmount,
            'fType': ProductType.query.filter_by(id=self.fType).first().serialize(),
            'fUM': ProductUM.query.filter_by(id=self.fUM).first().serialize(),
            'fCompany': Company.query.filter_by(id=self.fCompany).first().serialize()
            }
