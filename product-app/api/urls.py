# -*- coding: utf-8 -*-
'''
    RESTful API for the Django Models
'''

from django.conf.urls.defaults import *
from piston.resource import Resource
from champloo.api import handlers
from emmiters import ExtJSONEmitter

productum_resource = Resource(handlers.ProductUMHandler)
products_resource = Resource(handlers.ProductsHandler)
companies_resource = Resource(handlers.CompaniesHandler)

urlpatterns = patterns('',
   url(r'^productum/(?P<id>\d+)$', productum_resource, {'emitter_format': 'ext-json'}),
   url(r'^productum$', productum_resource, {'emitter_format': 'ext-json'}),
   url(r'^products/(?P<id>\d+)$', products_resource, {'emitter_format': 'ext-json'}),
   url(r'^products$', products_resource, {'emitter_format': 'ext-json'}),
   url(r'^companies/(?P<id>\d+)$', companies_resource, {'emitter_format': 'ext-json'}),
   url(r'^companies$', companies_resource, {'emitter_format': 'ext-json'}),
)