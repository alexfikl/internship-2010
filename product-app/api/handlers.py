# -*- coding: utf-8 -*-
from django.utils import simplejson

from piston.handler import BaseHandler
from piston.utils import rc, require_mime, require_extended, validate

from champloo.samurai.models import *

class ProductUMHandler(BaseHandler):
    '''
        Piston handler for the ProductUM Class
    '''
    fields = ('sUMName', 'nFactor')
    model = ProductUM

    def create(self, request, *args, **kwargs):
        '''
            CRUD function rewrited to add a ProductUM
        '''
        if not self.has_model():
            return rc.NOT_IMPLEMENTED

        attrs = self.flatten_dict(request.POST)
        if attrs.has_key('data'):
            ext_posted_data = simplejson.loads(request.POST.get('data'))
            attrs = self.flatten_dict(ext_posted_data)
            logging.debug(attrs)

        try:
            inst = self.model.objects.get(**attrs)
            return rc.DUPLICATE_ENTRY
        except self.model.DoesNotExist:
            inst = self.model(**attrs)
            inst.save()
            return inst
        except self.model.MultipleObjectsReturned:
            return rc.DUPLICATE_ENTRY

    def update(self, request, id):
        '''
            CRUD function rewrited to update and existing ProductUM
        '''
        if not self.has_model():
            return rc.NOT_IMPLEMENTED

        attrs = self.flatten_dict(request.POST)

        print attrs

        inst = self.model.objects.get(id=id)
        inst.sUMName = attrs['sUMName']
        inst.nFactor = attrs['nFactor']
        inst.save()

        return inst

class ProductsHandler(BaseHandler):
    '''
        Piston handler for the Products models.
        Rewrites create and update.
        used fields: ('id', ('sProductUM', ('id','sUMName', 'nFactor')), ('sProductType', ('id','sProductTypeName', 'sProductTypeDescription', 'nProductTypeDepth')))
    '''
    fields = ('id', ('sProductUM', ('id','sUMName', 'nFactor')), ('sProductType', ('id','sProductTypeName', 'sProductTypeDescription', 'nProductTypeDepth')))
    model = Products

    def create(self, request, *args, **kwargs):
         '''
            CRUD function rewriten to add Products
         '''
         if not self.has_model():
              return rc.NOT_IMPLEMENTED

         print 'entered create'
         attrs = self.flatten_dict(request.POST)
         print attrs

         puattrs = { 'sUMName': attrs['sUnit'], 'nFactor': int(attrs['nFactor']) }
         ptattrs = { 'sProductTypeName': attrs['sName'], 'sProductTypeDescription': attrs['sDescription'], 'nProductTypeDepth': int(attrs['nDepth']) }

         print puattrs
         print ptattrs

         try:
            punit = ProductUM.objects.get(**puattrs)
         except ProductUM.DoesNotExist:
            punit = ProductUM(**puattrs)
            punit.save()

         print 'made unit'

         try:
            ptype = ProductTypes.objects.get(**ptattrs)
         except ProductTypes.DoesNotExist:
            ptype = ProductTypes(**ptattrs)
            ptype.save()

         print 'made type'
         pattrs = {'sProductUM': punit, 'sProductType': ptype}

         try:
            inst = self.model.objects.get(**pattrs)
            return rc.DUPLICATE_ENTRY
         except self.model.DoesNotExist:
            inst = self.model(**pattrs)
            inst.save()
            return inst
         except self.model.MultipleObjectsReturned:
            return rc.DUPLICATE_ENTRY

    def update(self, request, id):
         if not self.has_model():
              return rc.NOT_IMPLEMENTED

         print 'entered update'
         attrs = self.flatten_dict(request.POST)
         print attrs
         print self.flatten_dict(request.PUT)

         if attrs.has_key('data'):
            ext_posted_data = simplejson.loads(request.POST.get('data'))
            attrs = self.flatten_dict(ext_posted_data)

         ptype = ProductTypes.objects.get(id=attrs['tid'])
         ptype.sProductTypeName = attrs['sName']
         ptype.nProductTypeDepth = int(attrs['nDepth'])
         ptype.sProductTypeDescription = attrs['sDescription']
         ptype.save()

         punit = ProductUM.objects.get(id=attrs['uid'])
         punit.sUMName = attrs['sUnit']
         punit.nFactor = int(attrs['nFactor'])
         punit.save()

         inst = self.model.objects.get(id=id)
         inst.sProductType = ptype
         inst.sProductUM = punit
         inst.save()

         return inst



class CompaniesHandler(BaseHandler):
    '''
        Piston Handler for the Companies Model
    '''
    fields = ('id', ('sCompanyType', ('id','sCompanyTypeName',)), 'sCompanyName', 'sRegCom', 'sCUI')
    model = Companies

    def create(self, request, *args, **kwargs):
        if not self.has_model():
            return rc.NOT_IMPLEMENTED

        attrs = self.flatten_dict(request.POST)
        print attrs

        ctattrs = {'sCompanyTypeName': attrs['sType']}

        try:
            ctype = CompanyType.objects.get(**ctattrs)
        except CompanyType.DoesNotExist:
            ctype = CompanyType(**ctattrs)
            ctype.save()

        print 'made type'
        cattrs = {'sCompanyType': ctype, 'sCompanyName': attrs['sName'], 'sRegCom': attrs['sReg'], 'sCUI':attrs['sCUI']}

        try:
            inst = self.model.objects.get(**cattrs)
            print 'new one'
            return rc.DUPLICATE_ENTRY
        except self.model.DoesNotExist:
            inst = self.model(**cattrs)
            print 'created'
            inst.save()
            return inst
        except self.model.MultipleObjectsReturned:
            print 'duplicate'
            return rc.DUPLICATE_ENTRY

    def update(self, request, id):
         if not self.has_model():
              return rc.NOT_IMPLEMENTED

         print 'entered update'
         attrs = self.flatten_dict(request.POST)
         if attrs.has_key('data'):
            ext_posted_data = simplejson.loads(request.POST.get('data'))
            attrs = self.flatten_dict(ext_posted_data)

         ctype = CompanyType.objects.get(id=attrs['tid'])
         ctype.sCompanyTypeName = attrs['sType']
         ctype.save()

         inst = self.model.objects.get(id=id)
         inst.sCompanyType = ctype
         inst.sCompanyName = attrs['sName']
         inst.sRegCom = attrs['sReg']
         inst.sCUI = attrs['sCUI']