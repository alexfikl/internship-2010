        var productreader = new Ext.data.JsonReader({
            totalProperty: 'total',
            successProperty: 'success',
            idProperty: 'id',
            messageProperty: 'message',
            root: 'data',
            fields: [{
                name: 'id',
                mapping: 'id',
                allowBlank: false
            },
            {
                name: 'sUMName',
                mapping: 'sProductUM.sUMName',
                allowBlank: false
            },
            {
                name: 'nFactor',
                mapping: 'sProductUM.nFactor',
                allowBlank: false
            },
            {
                name: 'sType',
                mapping: 'sProductType.sProductTypeName',
                allowBlank: false
            },
            {
                name: 'sDescription',
                mapping: 'sProductType.sProductTypeDescription'
            },
            {
                name: 'nDepth',
                mapping: 'sProductType.nProductTypeDepth'
            }]
        });

        var companiesreader = new Ext.data.JsonReader({
            totalProperty: 'total',
            successProperty: 'success',
            idProperty: 'id',
            messageProperty: 'message',
            root: 'data',
            fields: [{
                name: 'id',
                mapping: 'id',
                allowBlank: false
            },
            {
                name: 'sName',
                mapping: 'sCompanyName',
                allowBlank: false
            },
            {
                name: 'sType',
                mapping: 'sCompanyType.sCompanyTypeName',
                allowBlank: false
            },
            {
                name: 'sReg',
                mapping: 'sRegCom',
                allowBlank: false
            },
            {
                name: 'sCUI',
                mapping: 'sCUI'
            }]
        });

        // The new DataWriter component.
        var writer = new Ext.data.JsonWriter({
            encode: true,
            //false // <-- don't return encoded JSON -- causes Ext.Ajax#request to send data using jsonData config rather than HTTP params
            writeAllFields: true
        });

        // Typical Store collecting the Proxy, Reader and Writer together.
        var productstore = new Ext.data.Store({
            id: 'products',
            restful: true, // <-- This Store is RESTful
            proxy: {
                type: 'ajax',
                url : '/api/products',
                reader: productreader
            },
            writer: writer
        });

        var companiestore = new Ext.data.Store({
            id: 'companies',
            restful: true,
            // <-- This Store is RESTful
            proxy:{
                type: 'ajax',
                url : '/api/companies',
                reader: companiesreader
            },
            writer: writer
        });

        // Let's pretend we rendered our grid-columns with meta-data from our ORM framework.
        var productColumnModel = new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            header: "Name",
            width: 80,
            sortable: true,
            dataIndex: 'sType',
            editor: new Ext.form.TextField({})
        },
        {
            header: "Amount",
            width: 50,
            sortable: true,
            dataIndex: 'nFactor',
            editor: new Ext.form.TextField({})
        },
        {
            header: "Unit",
            width: 50,
            sortable: true,
            dataIndex: 'sUMName',
            editor: new Ext.form.TextField({})
        },
        {
            header: "Description",
            width: 50,
            sortable: true,
            dataIndex: 'sDescription',
            editor: new Ext.form.TextField({})
        },
        {
            header: "Depth",
            width: 50,
            sortable: true,
            dataIndex: 'nDepth',
            editor: new Ext.form.TextField({})
        }]);

        var companiesColumnModel = new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            header: "Name",
            width: 80,
            sortable: true,
            dataIndex: 'sName',
            editor: new Ext.form.TextField({})
        },
        {
            header: "Type",
            width: 50,
            sortable: true,
            dataIndex: 'sType',
            editor: new Ext.form.TextField({})
        },
        {
            header: "Region",
            width: 50,
            sortable: true,
            dataIndex: 'sReg',
            editor: new Ext.form.TextField({})
        },
        {
            header: "Information",
            width: 50,
            sortable: true,
            dataIndex: 'sCUI',
            editor: new Ext.form.TextField({})
        }]);

        // load the stores immediately
        productstore.load();

        companiestore.load();

        var produse = new Ext.Container({
            layout: 'card',
            title: 'Produse',
            activeItem: 0,
            items: [
            new Ext.FormPanel({
                url: '/api/products',
                name: true,
                itemId: 'pform',
                autoHeight: true,
                items: [
                new Ext.form.TextField({
                    fieldLabel: 'Name',
                    name: 'sType',
                    itemId: 'focus',
                    width: 300,
                    allowBlank: false
                }), new Ext.form.TextField({
                    fieldLabel: 'Description',
                    name: 'sDescription',
                    width: 300,
                    allowBlank: false
                }), new Ext.form.TextField({
                    fieldLabel: 'Depth',
                    name: 'nDepth',
                    width: 300,
                    allowBlank: false
                }), new Ext.form.TextField({
                    fieldLabel: 'Unit',
                    name: 'sUMName',
                    width: 300,
                    allowBlank: false
                }), new Ext.form.TextField({
                    fieldLabel: 'Factor',
                    name: 'nFactor',
                    width: 300,
                    allowBlank: false
                })],
                buttons: [{
                    text: 'Save',
                    formBind: true,
                    handler: onProductSave,
                    scope: this
                },
                {
                    text: 'Cancel',
                    scope: this,
                    handler: onCancel
                }]
            }), new Ext.grid.GridPanel({
                itemId: 'pgrid',
                autoScroll: true,
                store: productstore,
                cm: productColumnModel,
                viewConfig: {
                    forceFit: true
                },
                listeners: {
                    cellclick: function (grid, idx, e) {},
                    rowclick: function (grid, idx, e) {},
                    containerclick: function (grid, idx, e) {}
                }
            })]
        });

        var companii = new Ext.Container({
            layout: 'card',
            title: 'Companii',
            activeItem: 0,
            items: [
            new Ext.FormPanel({
                url: '/api/companies',
                name: true,
                autoHeight: true,
                itemId: 'cform',
                frame: true,
                items: [
                new Ext.form.TextField({
                    fieldLabel: 'Name',
                    name: 'sName',
                    itemId: 'focus',
                    width: 300,
                    allowBlank: false
                }), new Ext.form.TextField({
                    fieldLabel: 'Type',
                    name: 'sType',
                    width: 300,
                    allowBlank: false
                }), new Ext.form.TextField({
                    fieldLabel: 'Region',
                    name: 'sReg',
                    width: 300,
                    allowBlank: false
                }), new Ext.form.TextField({
                    fieldLabel: 'CUI',
                    name: 'sCUI',
                    width: 300,
                    allowBlank: false
                })],
                buttons: [{
                    text: 'Save',
                    formBind: true,
                    handler: onCompanySave,
                    scope: this
                },
                {
                    text: 'Cancel',
                    scope: this,
                    handler: onCancel
                }]
            }), new Ext.grid.GridPanel({
                itemId: 'cgrid',
                autoScroll: true,
                store: companiestore,
                cm: companiesColumnModel,
                viewConfig: {
                    forceFit: true
                },
                listeners: {
                    cellclick: function (grid, idx, e) {},
                    rowclick: function (grid, idx, e) {},
                    containerclick: function (grid, idx, e) {}
                }
            })]
        });


        productGrid = produse.getComponent('pgrid');
        companyGrid = companii.getComponent('cgrid');

        var panel = new Ext.TabPanel({
            fullscreen: true,
            animation: 'slide',
            items: [produse, companii]
        });

        var tabBar = panel.getTabBar();
        tabBar.addDocked({
            xtype: 'button',
            ui: 'mask',
            iconCls: 'add',
            dock: 'right',
            stretch: false,
            align: 'center',
            handler: onAdd
        });

        tabBar.addDocked({
            xtype: 'button',
            ui: 'mask',
            iconCls: 'delete',
            dock: 'right',
            stretch: false,
            align: 'center',
            handler: onDelete
        });

        function onProductSave() {
          panel.getActiveItem().getLayout().getActiveItem().getForm().submit({
               url: '/api/products',
               method: 'POST',
               success: function(form, action){
                                  var u = new productGrid.store.recordType({
                                        sUMName: form.findField('sUMName').getValue(),
                                        nFactor: form.findField('nFactor').getValue(),
                                        sType: form.findField('sType').getValue(),
                                        sDescription: form.findField('sDescription').getValue(),
                                        nDepth: form.findField('nDepth').getValue(),
                                  });

                                  produse.getLayout().setActiveItem('pgrid');
                                  productGrid.store.insert(productGrid.store.getTotalCount(), u);
                        },
               failure: function(form, action) {
                               switch (action.failureType) {
                                      case Ext.form.Action.CLIENT_INVALID:
                                           Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                           break;
                                      case Ext.form.Action.CONNECT_FAILURE:
                                           Ext.Msg.alert('Failure', 'Ajax communication failed');
                                           break;
                                      case Ext.form.Action.SERVER_INVALID:
                                           Ext.Msg.alert('Failure', action.result.msg);
                               }
                        }
          });
     }

    function onCompanySave() {
           panel.getActiveItem().getLayout().getActiveItem().getForm().submit({
               url: '/api/companies',
               method: 'POST',
               success: function(form, action){
                                  var u = new companyGrid.store.recordType({
                                        sName: form.findField('sName').getValue(),
                                        sReg: form.findField('sReg').getValue(),
                                        sType: form.findField('sType').getValue(),
                                        sCUI: form.findField('sCUI').getValue()
                                  });

                                  produse.getLayout().setActiveItem('cgrid');
                                  companyGrid.store.insert(companyGrid.store.getTotalCount(), u);
                        },
               failure: function(form, action) {
                               switch (action.failureType) {
                                      case Ext.form.Action.CLIENT_INVALID:
                                           Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                           break;
                                      case Ext.form.Action.CONNECT_FAILURE:
                                           Ext.Msg.alert('Failure', 'Ajax communication failed');
                                           break;
                                      case Ext.form.Action.SERVER_INVALID:
                                           Ext.Msg.alert('Failure', action.result.msg);
                               }
                        }
          });
    }

    function onCancel() {
          panel.getActiveItem().getLayout().getActiveItem().getForm().reset();
          produse.getLayout().setActiveItem(0);
    }

    function onAdd(btn, ev) {
        panel.getActiveItem().getLayout().setActiveItem(1);
        panel.getActiveItem().getLayout().getActiveItem().getComponent('focus').focus(true);
    }

    function onDelete() {

        var rec = panel.getActiveItem().getLayout().getActiveItem().getSelectionModel().getSelected();
        if (!rec) {
            return false;
        }
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete?', showResult);
    }

    function showResult(btn){
          if (btn == 'yes'){
               var rec = panel.getActiveItem().getLayout().getActiveItem().getSelectionModel().getSelected();
               panel.getActiveItem().getLayout().getActiveItem().store.remove(rec)
               panel.getActiveItem().getLayout().getActiveItem().store.reload();
          }
    }
    }
