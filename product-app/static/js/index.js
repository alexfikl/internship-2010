Ext.setup({
    icon: 'icon.png',
    tabletStartupScreen: 'tablet_startup.png',
    phoneStartupScreen: 'phone_startup.png',
    glossOnIcon: false,
    onReady: function() {
        var formBase = {
            scroll: 'vertical',
            url   : '/api/products',
            standardSubmit : false,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'New Product',
                    instructions: 'Please enter the information above.',
                    defaults: {
                        required: true,
                        labelAlign: 'left'
                    },
                    items: [{
                        xtype: 'textfield',
                        name : 'name',
                        label: 'Name',
                        autoCapitalize : false
                    }, {
                        xtype: 'spinnerfield',
                        name : 'depth',
                        label: 'Depth',
                        value: 0
                    }, {
                        xtype: 'textfield',
                        name : 'unit',
                        label: 'Unit'
                    }, {
                        xtype: 'spinnerfield',
                        name : 'factor',
                        label: 'Factor',
                        value: 0
                    }, {
                        xtype: 'textarea',
                        name : 'description',
                        label: 'Description',
                        maxRows: 5,
                        maxLength: 100
                    }]
                }
            ],
            listeners : {
                submit : function(form, result){
                    console.log('success', Ext.toArray(arguments));
                },
                exception : function(form, result){
                    console.log('failure', Ext.toArray(arguments));
                }
            },

            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {xtype: 'spacer'},
                        {
                            text: 'Save',
                            ui: 'action',
                            handler: function() {
                                if(formBase.user){
                                    form.updateModel(formBase.user, true);
                                }
                                form.submit({
                                    waitMsg : {message:'Submitting', cls : 'demos-loading'}
                                });
                            }
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                form.reset();
                            }
                        }
                    ]
                }
            ]
        };

        if (Ext.platform.isAndroidOS) {
            formBase.items.unshift({
                xtype: 'component',
                styleHtmlContent: true,
                html: '<span style="color: red">Forms on Android are currently under development. We are working hard to improve this in upcoming releases.</span>'
            });
        }

        if (Ext.platform.isPhone) {
            formBase.fullscreen = true;
        } else {
            Ext.apply(formBase, {
                autoRender: true,
                floating: true,
                modal: true,
                centered: true,
                hideOnMaskTap: false,
                height: 500,
                width: 480
            });
        }

        form = new Ext.form.FormPanel(formBase);
        form.show();
    }
});