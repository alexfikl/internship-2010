Ext.BLANK_IMAGE_URL = '/static/vendor/extjs/resources/images/default/s.gif';

var productReader = new Ext.data.JsonReader({
    totalProperty: 'total',
    successProperty: 'success',
    idProperty: 'id',
    messageProperty: 'message',
    root: 'data',
    fields: [
    {
        name: 'id',
        mapping: 'id',
        allowBlank: false
    },
    {
        name: 'sName',
        mapping: 'sName',
        allowBlank: false
    },
    {
        name: 'nPrice',
        mapping: 'nPrice',
        allowBlank: false
    },
    {
        name: 'nAmount',
        mapping: 'nAmount',
        allowBlank: false
    },
    {
        name: 'uid',
        mapping: 'fUM.id',
        allowBlank: false
    },
    {
        name: 'sUnit',
        mapping: 'fUM.sName',
        allowBlank: false
    },
    {
        name: 'sUnitShort',
        mapping: 'fUM.sShort',
        allowBlank: false
    },
    {
        name: 'tid',
        mapping: 'fType.id',
        allowBlank: false
    },
    {
        name: 'sCategory',
        mapping: 'fType.sName',
        allowBlank: false
    },
    {
        name: 'cid',
        mapping: 'fCompany.id',
        allowBlank: false
    },
    {
        name: 'sCompanyName',
        mapping: 'fCompany.sName',
        allowBlank: false
    },
    {
        name: 'sCompanyType',
        mapping: 'fCompany.sType',
        allowBlank: false
    }]
});

var companiesReader = new Ext.data.JsonReader({
    totalProperty: 'total',
    successProperty: 'success',
    idProperty: 'id',
    messageProperty: 'message',
    root: 'data',
    fields: [
    {
        name: 'id',
        mapping: 'id',
        allowBlank: false
    },
    {
        name: 'sName',
        mapping: 'sName',
        allowBlank: false
    },
    {
        name: 'sType',
        mapping: 'sType',
        allowBlank: false
    },
    {
        name: 'sReg',
        mapping: 'sLocation',
        allowBlank: false
    },
    {
        name: 'sCUI',
        mapping: 'sCUI'
    }]
});

var writer = new Ext.data.JsonWriter({
    encode: true,
    writeAllFields: true
});

var productStore = new Ext.data.Store({
    id: 'products',
    restful: true,
    proxy: new Ext.data.HttpProxy({
        url: '/api/product'
    }),
    reader: productReader,
    writer: writer
});

var companieStore = new Ext.data.Store({
    id: 'company',
    restful: true,
    proxy: new Ext.data.HttpProxy({
        url: '/api/company'
    }),
    reader: companiesReader,
    writer: writer
});

var productColumnModel = new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
        header: "Name",
        width: 80,
        sortable: true,
        dataIndex: 'sName'
    },
    {
        header: "Amount",
        width: 50,
        sortable: true,
        dataIndex: 'nAmount'
    },
    {
        header: "Unit",
        width: 50,
        sortable: true,
        dataIndex: 'sUnitShort'
    },
    {
        header: "Description",
        width: 50,
        sortable: true,
        dataIndex: 'sCategory'
    },
    {
        header: "Company",
        width: 50,
        sortable: true,
        dataIndex: 'sCompanyName'
    }
]);

var companiesColumnModel = new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
        header: "Name",
        width: 80,
        sortable: true,
        dataIndex: 'sName'
    },
    {
        header: "Type",
        width: 50,
        sortable: true,
        dataIndex: 'sType'
    },
    {
        header: "Location",
        width: 50,
        sortable: true,
        dataIndex: 'sReg'
    },
    {
        header: "Unique ID",
        width: 50,
        sortable: true,
        dataIndex: 'sCUI'
    }
]);

productStore.load();
companieStore.load();

Ext.onReady(function () {
    Ext.QuickTips.init();

    var mainToolbar = new Ext.Toolbar({
        itemId: 'maintool',
        height: 32,
        margins: '0 0 5 0',
        items: [
        {
            xtype: 'spacer'
        },
        {
            xtype: 'button',
            text: 'View Tables',
            menu: [{
                text: 'Products',
                iconCls: 'silk-information',
                handler: selectProducts
            },
            {
                text: 'Companies',
                iconCls: 'silk-information',
                handler: selectCompanies
            }]
        },
        {
            xtype: 'tbfill'
        },
        {
            xtype: 'button',
            text: 'Log out'
        }]
    });

    var editToolbar = new Ext.Toolbar({
        itemId: 'edittool',
        height: 32,
        items: [{
            xtype: 'button',
            itemId: 'crumbs',
            handleMouseEvents: false,
            text: ''
        },
        {
            xtype: 'tbfill'
        },
        {
            xtype: 'button',
            text: 'New',
            iconCls: 'silk-add',
            handler: onEdit
        },
        {
            xtype: 'spacer'
        },
        {
            xtype: 'button',
            text: 'Edit',
            iconCls: 'silk-application-view-list',
            handler: onEdit
        },
        {
            xtype: 'spacer'
        },
        {
            xtype: 'button',
            text: 'Delete',
            iconCls: 'silk-delete',
            handler: onDelete
        }]
    });

    var productSearchForm = new Ext.form.FormPanel({
        title: 'Search Product',
        itemId: 'psearchform',
        frame: true,
        autoHeight: true,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'ID',
            name: 'id',
            width: 300
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Name',
            name: 'sName',
            width: 300
        },
        {
            xtype: 'combo',
            fieldLabel: 'Category',
            name: 'sCategory',
            width: 300
        },
        {
            xtype: 'fieldset',
            title: 'Others',
            collapsible: true,
            collapsed: true,
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Company',
                width: 300
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Price',
                width: 300,
                name: 'nFactor'
            }]
        }],
        buttons: [{
            text: 'Search',
            scope: this,
            handler: onSearch
        }]
    });

    var companySearchForm = new Ext.form.FormPanel({
        title: 'Search Company',
        itemId: 'csearchform',
        frame: true,
        autoHeight: 300,
        items: [{
            xtype: 'textfield',
            fieldLabel: 'CUI',
            name: 'sCUI',
            width: 300
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Nume',
            name: 'sName',
            width: 300
        },
        {
            xtype: 'combo',
            fieldLabel: 'Categorie',
            name: 'sType',
            width: 300
        },
        {
            xtype: 'fieldset',
            title: 'Alte Filtre',
            collapsible: true,
            collapsed: true,
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Regiune',
                name: 'sReg',
                width: 300
            }]
        }],
        buttons: [{
            text: 'Search',
            scope: this,
            handler: onSearch
        }]
    });

    var productEditForm = new Ext.form.FormPanel({
        url: '/api/product',
        itemId: 'peditform',
        title: 'Edit Product',
        autoHeight: true,
        frame: true,
        standardSubmit:false,
        defaults:{
            xtype: 'textfield',
            allowBlank: true,
            width: 300
        },
        items: [
            { xtype: 'hidden', name: 'id'},
            { xtype: 'hidden', name: 'uid' },
            { xtype: 'hidden', name: 'tid' },
            { fieldLabel: 'Name', itemId: 'focus', name: 'sName' },
            { fieldLabel: 'Depth', name: 'nDepth' },
            { fieldLabel: 'Unit', name: 'sUnit' },
            { fieldLabel: 'Factor', name: 'nFactor' },
            { fieldLabel: 'Description', name: 'sDescription' }
        ],
        buttons: [{
            text: 'Save',
            formBind: true,
            handler: onSave,
            scope: this
        },
        {
            text: 'Cancel',
            scope: this,
            handler: onCancel
        }]
    });

    var companyEditForm = new Ext.form.FormPanel({
        url: '/api/company',
        autoHeight: true,
        itemId: 'ceditform',
        title: 'Edit Company',
        frame: true,
        items: [
        new Ext.form.TextField({
            fieldLabel: 'Name',
            name: 'sName',
            itemId: 'focus',
            width: 300
        }), new Ext.form.TextField({
            fieldLabel: 'Type',
            name: 'sType',
            width: 300
        }), new Ext.form.TextField({
            fieldLabel: 'Region',
            name: 'sReg',
            width: 300,
            allowBlank: false
        }), new Ext.form.TextField({
            fieldLabel: 'CUI',
            name: 'sCUI',
            width: 300,
            allowBlank: false
        })],
        buttons: [{
            text: 'Save',
            formBind: true,
            handler: onSave,
            scope: this
        },
        {
            text: 'Cancel',
            scope: this,
            handler: onCancel
        }]
    });

    var generalGrid = new Ext.grid.GridPanel({
        region: 'center',
        itemId: 'grid',
        iconCls: 'icon-grid',
        frame: true,
        title: 'Table',
        autoScroll: true,
        width: 500,
        store: new Ext.data.Store({}),
        cm: new Ext.grid.ColumnModel({}),
        viewConfig: {
            forceFit: true
        }
    });

    var searchCards = new Ext.Container({
        layout: 'card',
        activeItem: 0,
        margins: '0 0 10 0',
        items: [productSearchForm, companySearchForm]
    });

    var editCards = new Ext.Container({
        layout: 'card',
        activeItem: 0,
        items: [productEditForm, companyEditForm]
    });

    var forms = new Ext.Container({
        region: 'west',
        layout: 'vbox',
        align: 'strech',
        width: 500,
        margins: '0 10 0 0',
        items: [searchCards, editCards]
    });

    var tools = new Ext.Container({
        region: 'north',
        layout: 'vbox',
        height: 77,
        items: [mainToolbar, editToolbar]
    });

    new Ext.Viewport({
        layout: 'border',
        items: [tools, forms, generalGrid]
    }).show();

    var breadcrumb = editToolbar.getComponent('crumbs');
    editCards.hide();

    function selectProducts() {
        breadcrumb.setText("Menu > Products > Product List");
        generalGrid.reconfigure(productStore, productColumnModel);
        generalGrid.setTitle("Product List");

        editCards.getLayout().setActiveItem('peditform');
        searchCards.getLayout().setActiveItem('psearchform');

        editCards.doLayout(true);
        searchCards.doLayout(true);
    }

    function selectCompanies() {
        breadcrumb.setText("Menu > Companies > Company List");
        generalGrid.reconfigure(companieStore, companiesColumnModel);
        generalGrid.setTitle("Company List");

        editCards.getLayout().setActiveItem('ceditform');
        searchCards.getLayout().setActiveItem('csearchform');

        editCards.doLayout(true);
        searchCards.doLayout(true);
    }

    function onSave() {
        var form = editCards.getLayout().activeItem.getForm();
        var values = form.getValues();
        var url = '/api/';
        var method = 'POST';

        console.log(values.sDescription, values.nDepth);

        if (breadcrumb.getText().toLowerCase().indexOf('product') != -1) {
            url += 'product';
        }
        else {
            url += 'company';
        }

        if (values.id) {
            console.log('has id..updating..')
            url += '/' + values.id;
            method = 'PUT';
        }

        form.submit({
            url: url,
            method: method,
            success: function () {
                Ext.Msg.alert('Success', 'Item has been successfully ' + (method == 'PUT' ? 'updated.' : 'added to the database.'));
            },
            failure: function(form, action) {
                               switch (action.failureType) {
                                      case Ext.form.Action.CLIENT_INVALID:
                                           Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                           break;
                                      case Ext.form.Action.CONNECT_FAILURE:
                                           Ext.Msg.alert('Failure', 'Ajax communication failed');
                                           break;
                                      case Ext.form.Action.SERVER_INVALID:
                                           Ext.Msg.alert('Failure', action.result.msg);
                               }
                        }
        });
        console.log(editCards.getLayout().activeItem.getForm().getValues());
        generalGrid.store.load();
        form.reset();
        editCards.hide();
    }

    function onSearch() {
        Ext.Msg.alert('Search', 'Not Implemented');
    }

    function onCancel() {
        editCards.getLayout().activeItem.getForm().reset();
        editCards.hide();
        if ((breadcrumb.getText().toLowerCase().indexOf('product') != -1)) {
            breadcrumb.setText("Menu > Products > Product List");
        }
        else {
            breadcrumb.setText("Menu > Companies > Company List");
        }
    }

    function onEdit(btn, ev) {
        if (!breadcrumb.getText()) {
            return false;
        }

        if (!editCards.isVisible()) {
            editCards.show();
        }
        else {
            editCards.hide();
            if ((breadcrumb.getText().toLowerCase().indexOf('product') != -1)) {
                breadcrumb.setText("Menu > Products > Product List");
            }
            else {
                breadcrumb.setText("Menu > Companies > Company List");
            }

            return false;
        }

        if ((breadcrumb.getText().toLowerCase().indexOf('product') != -1)) {
            editCards.getLayout().setActiveItem('peditform');
            breadcrumb.setText("Menu > Products > Edit Product");
        }
        else {
            editCards.getLayout().setActiveItem('ceditform');
            breadcrumb.setText("Menu > Companies > Edit Company");
        }

        if (btn.getText() == 'Edit') {
            var rec = generalGrid.getSelectionModel().getSelected();
            editCards.getLayout().activeItem.getForm().loadRecord(rec);
        }

        editCards.getLayout().activeItem.get('focus').focus(true);
    }

    function onDelete() {
        var rec = generalGrid.getSelectionModel().getSelected();
        if (!rec) {
            return false;
        }
        console.log(rec.json.sName);
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete?' + rec.sName, result);
    }

    function result(btn) {
        if (btn == 'yes') {
            generalGrid.store.remove(rec);
            generalGrid.store.reload();
        }
    }

});
