/*
    templates for the list view
*/

var productTemplate = new Ext.XTemplate(
'<tpl for="."><div class="product"><div class="name">{sName}</div>',
'<p><table class="info" border="0"><tr><td class="def">Depth:</td><td>{nDepth}</td></tr>',
'<tr><td class="def">Unit:</td><td>{sUnit}</td></tr>',
'<tr><td class="def">Factor:</td><td>{nFactor}</td></tr>',
'<tr><td class="def">Description:</td><td width="250">{sDescription}</td></tr></table></p></div></tpl>'
);

var companyTemplate = new Ext.XTemplate(
'<tpl for="."><div class="company"><div class="name">{sName}</div>',
'<p><table class="info" border="0"><tr><td class="def">Type:</td><td>{sType}</td></tr>',
'<tr><td class="def">Region:</td><td>{sReg}</td></tr>',
'<tr><td class="def">CUI:</td><td>{sCUI}</td></tr></table></p></div></tpl>'
);

/*
    store models
*/

Ext.regModel('Product',{
    fields: [
        {name: 'id', mapping: 'id', allowBlank:false},
        {name: 'uid', mapping: 'sProductUM.id', allowBlank:false},
        {name: 'tid', mapping: 'sProductType.id', allowBlank:false},
        {name: 'sUnit', mapping:'sProductUM.sUMName', allowBlank: false },
        {name: 'nFactor', mapping: 'sProductUM.nFactor', allowBlank: false},
        {name: 'sName', mapping: 'sProductType.sProductTypeName', allowBlank: false},
        {name: 'sDescription', mapping: 'sProductType.sProductTypeDescription', allowBlank: false},
        {name: 'nDepth', mapping: 'sProductType.nProductTypeDepth', allowBlank: false}
    ],
    store: productStore
});

Ext.regModel('Company', {
    fields: [
        {name: 'id', mapping: 'id', allowBlank:false},
        {name: 'tid', mapping: 'sCompanyType.id', allosBlank:false},
        {name: 'sName', mapping:'sCompanyName', allowBlank: false },
        {name: 'sType', mapping: 'sCompanyType.sCompanyTypeName', allowBlank: false},
        {name: 'sReg', mapping: 'sRegCom', allowBlank: false},
        {name: 'sCUI', mapping: 'sCUI', allowBlank: false}
    ],
    store: companyStore
});

/*
    store definitions
*/

var productStore = new Ext.data.JsonStore({
    model: 'Product',
    restful:true,
    proxy: {
        type: 'ajax',
        url: '/api/products',
        reader: {
            type: 'json',
            idProperty: 'id',
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'message',
        }
    },
    autoSave:true
});

var companyStore = new Ext.data.JsonStore({
    model: 'Company',
    restful: true,
    proxy: {
        type: 'ajax',
        url: '/api/companies',
        reader: {
            type: 'json',
            idProperty: 'id',
            root: 'data',
            totalProperty: 'total',
            successProperty: 'success',
            messageProperty: 'message',
        }
    },
    autoSave:true
});


// load stores now!
productStore.load();
companyStore.load();

Ext.setup({
    glossOnIcon: false,
    onReady: function () {

        /*
            template object for a the lists
        */

        var groupingBase = {
            tpl: productTemplate,
            itemSelector: 'div.product',

            singleSelect: true,

            components : function(record, node, index) {
                return new Ext.Container({

                })
            },
            disclosure: {
                iconCls: 'settings',
                handler: onEdit
            },
            store: productStore
        };

        /*
            create lists from template
        */

        var productList = new Ext.List(groupingBase);
        var companyList = new Ext.List(Ext.apply(groupingBase, {tpl: companyTemplate, itemSelector: 'div.company', store: companyStore}));

        /*
            template for forms
        */

        var formBase = {
            scroll: 'vertical',
            url   : '/api/products',
            standardSubmit : false,
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Product',
                    instructions: 'Please enter the information above.',
                    defaults: {
                        required: true,
                        labelAlign: 'left'
                    },
                    items: [{
                        xtype: 'hidden',
                        name: 'id'
                    },{
                        xtype: 'hidden',
                        name: 'uid'
                    },{
                        xtype: 'hidden',
                        name: 'tid'
                    },{
                        xtype: 'textfield',
                        name : 'sName',
                        label: 'Name',
                        autoCapitalize : false
                    }, {
                        xtype: 'spinnerfield',
                        name : 'nDepth',
                        label: 'Depth',
                        value: 0
                    }, {
                        xtype: 'textfield',
                        name : 'sUnit',
                        label: 'Unit'
                    }, {
                        xtype: 'spinnerfield',
                        name : 'nFactor',
                        label: 'Factor',
                        value: 0
                    }, {
                        xtype: 'textarea',
                        name : 'sDescription',
                        label: 'Description',
                        maxRows: 5,
                        maxLength: 100
                    }]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {xtype: 'spacer'},
                        {
                            text: 'Save',
                            ui: 'action',
                            handler: onSave
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                panel.getActiveItem().getLayout().getActiveItem().reset();
                                panel.getActiveItem().getLayout().setActiveItem(0);
                            }
                        }
                    ]
                }
            ]
        };

        /*
            create forms from template
        */

        productForm = new Ext.form.FormPanel(formBase);

        Ext.apply(formBase, {
            url: '/api/companies',
            items: [
                {
                    xtype: 'fieldset',
                    title: 'Company',
                    instructions: 'Please enter the information above.',
                    defaults: {
                        required: true,
                        labelAlign: 'left'
                    },
                    items: [{
                        xtype: 'hidden',
                        name: 'id'
                    },{
                        xtype: 'hidden',
                        name: 'tid'
                    },{
                        xtype: 'textfield',
                        name : 'sName',
                        label: 'Name',
                        autoCapitalize : false
                    }, {
                        xtype: 'textfield',
                        name : 'sType',
                        label: 'Type'
                    }, {
                        xtype: 'textfield',
                        name : 'sReg',
                        label: 'Region'
                    }, {
                        xtype: 'textfield',
                        name : 'sCUI',
                        label: 'CUI'
                    }]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {xtype: 'spacer'},
                        {
                            text: 'Save',
                            ui: 'action',
                            handler: onSave
                        },
                        {
                            text: 'Cancel',
                            handler: function() {
                                panel.getActiveItem().getLayout().getActiveItem().reset();
                                panel.getActiveItem().getLayout().setActiveItem(0);
                            }
                        }
                    ]
                }
            ]
        });

        companyForm = new Ext.form.FormPanel(formBase);

        /*
            create tabs for the tabpanel
        */

        var produse = new Ext.Container({
            layout: 'card',
            title: 'Products',
            animation: 'slide',
            activeItem: 0,
            items: [productList , productForm]
        });

        var companii =  new Ext.Container({
            layout: 'card',
            title: 'Companies',
            animation: 'slide',
            activeItem: 0,
            items: [companyList, companyForm]
        });

        /*
            tabpanel
        */

        var panel = new Ext.TabPanel({
            animation: 'slide',
            autoRender: true,
            floating: true,
            centered: true,
            modal: true,
            hideOnMaskTap: false,
            height: 600,
            width: 480,
            items: [produse, companii]
        });

        //panel.render();
        panel.show();


        /*
            Add 'NEW' and 'DELETE' buttons on the TabPanel
        */
        var tabBar = panel.getTabBar();
        tabBar.addDocked({
            xtype: 'button',
            ui: 'mask',
            iconCls: 'add',
            dock: 'right',
            stretch: false,
            align: 'center',
            handler: onAdd
        });

        tabBar.addDocked({
            xtype: 'button',
            ui: 'mask',
            iconCls: 'delete',
            dock: 'right',
            stretch: false,
            align: 'center',
            handler: onDelete
        });


        /*
         **************************************************************************
         *                              FUNCTIONS
         **************************************************************************
         */

        /*
            Function for save button on forms
        */

        function onSave() {
            var form = panel.getActiveItem().getLayout().getActiveItem();
            var store = panel.getActiveItem().getLayout().getNext(true).getStore();
            var values = form.getValues();
            var url = '/api/products';
            var method = 'POST';

            if (panel.getActiveItem().title.toLowerCase().indexOf('product') == -1){
                url = '/api/companies';
            }

            if (values.id) {
                url += '/' + values.id;
                method = 'PUT';
            }

            form.submit({
                url: url,
                method: method
            });

            form.reset();
            store.load();
            panel.getActiveItem().getLayout().setActiveItem(0);
        }

        /*
            Function used by the list disclosure button
            @ record - edited record
            @ button - dont know the point of this:-?
            @ index - in list
        */

        function onEdit(record, button, index) {
            panel.getActiveItem().getLayout().setActiveItem(1);
            panel.getActiveItem().getLayout().getActiveItem().load(record);
        }

        /*
            Functions used by the tabpanel docked items
        */

        function onAdd() {
            panel.getActiveItem().getLayout().setActiveItem(1);
        }

        function onDelete() {
            var list = panel.getActiveItem().getLayout().getActiveItem();
            var rec = list.getSelectedRecords()[0];
            var url = '/api/products/' + rec.get('id');

            if (panel.getActiveItem().title.toLowerCase().indexOf('product') == -1){
                url = '/api/companies/' + rec.get('id');
            }

            if(rec) {
                list.store.remove(rec);
                Ext.Ajax.request({
                    url: url,
                    method: 'DELETE'
                });
            }
        }

        /*
            THE END
        */
    }
});