/*!
 * Ext JS Library 3.0+
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
// Application instance for showing user-feedback messages.

// Ext.BLANK_IMAGE_URL = '/static/js/ext/resources/images/default/s.gif';

var App = new Ext.App({});

// Create a standard HttpProxy instance.
var proxy = new Ext.data.HttpProxy({
    url: '/api/productum'
});

// Typical JsonReader.  Notice additional meta-data params for defining the core attributes of your json-response
var reader = new Ext.data.JsonReader({
    totalProperty: 'total',
    successProperty: 'success',
    idProperty: 'id',
    messageProperty: 'message',
    root: 'data'
}, [
    {name: 'id', allowBlank: false },
    {name: 'sUMName', allowBlank: false},
    {name: 'nFactor', allowBlank: false}
]);

// The new DataWriter component.
var writer = new Ext.data.JsonWriter({
    encode: true,  //false // <-- don't return encoded JSON -- causes Ext.Ajax#request to send data using jsonData config rather than HTTP params
    writeAllFields: true
});

// Typical Store collecting the Proxy, Reader and Writer together.
var store = new Ext.data.Store({
    id: 'user',
    restful: true,     // <-- This Store is RESTful
    proxy: proxy,
    reader: reader,
    writer: writer,    // <-- plug a DataWriter into the store just as you would a Reader
    listeners: {
        write: function(store, action, result, response, rs) {
            App.setAlert(response.success, response.message);
        }
    }
});

// Let's pretend we rendered our grid-columns with meta-data from our ORM framework.
var userColumns =  [
    {header: "ID", width: 50, sortable: true, dataIndex: 'id'},
    {header: "Unit Name", width: 100, sortable: true, dataIndex: 'sUMName', editor: new Ext.form.TextField({})},
    {header: "Factor", width: 50, sortable: true, dataIndex: 'nFactor', editor: new Ext.form.TextField({})}
];

// load the store immeditately
store.load();

Ext.onReady(function() {
    Ext.QuickTips.init();

    // use RowEditor for editing
    var editor = new Ext.ux.grid.RowEditor({
        saveText: 'Update'
    });

    // Create a typical GridPanel with RowEditor plugin
    var userGrid = new Ext.grid.GridPanel({
        renderTo: 'user-grid',
        iconCls: 'icon-grid',
        frame: true,
        title: 'Unit List',
        autoScroll: true,
        height: 300,
        store: store,
        plugins: [editor],
        columns: userColumns,
        viewConfig: {
            forceFit: true
        },
        listeners: {
            rowdblclick: function (grid, idx, e){},
            containerclick: function(grid, idx, e){
               grid.getSelectionModel().clearSelections();
            }
       }
    });

    function onEdit(btn, ev){
          var selected = userGrid.getSelectionModel().getSelected();
          if (selected){
               editor.stopEditing();
               editor.startEditing(selected.get('id') - 1);
          }
    }

    function onAdd(btn, ev) {
                if (!form1.isVisible()) form1.show();
                form1.render('user-form');
            }

    function onSave() {}

    function onDelete() {
        var rec = userGrid.getSelectionModel().getSelected();
        if (!rec) {
            return false;
        }
        Ext.MessageBox.confirm('Confirm', 'Are you sure you want to delete?', showResult);
    }

    function showResult(btn){
          if (btn == 'yes'){
               var rec = userGrid.getSelectionModel().getSelected();
               userGrid.store.remove(rec)
          }
    }

    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

    var form1 = new Ext.FormPanel({
        url:'/api/productum',
        name:true,
        title: 'Add Unit Form',
        frame: true,
        width: 500,
        items :[
            {
                xtype: 'textfield',
                fieldLabel: 'Name',
                name: 'sUMName',
                allowBlank:false,
                width: 200
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Factor',
                allowBlank: false,
                name: 'nFactor'
            }
        ],
        buttons: [{
            text: 'Save',
            formBind: true,
            handler: function() {
                form1.getForm().submit({
                    url: '/api/productum',
                    method: 'PUT',
                    success: function(form, action){
                        var u = new userGrid.store.recordType({
                                id: userGrid.store.getTotalCount() + 1,
                                sUMName: form.findField('sUMName').getValue(),
                                nFactor: form.findField('nFactor').getValue()
                        });

                        userGrid.store.insert(userGrid.store.getTotalCount(), u);
                        //alert('yes');
                    },
                    failure: function(form, action) {
                         switch (action.failureType) {
                              case Ext.form.Action.CLIENT_INVALID:
                                   Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                   break;
                              case Ext.form.Action.CONNECT_FAILURE:
                                   Ext.Msg.alert('Failure', 'Ajax communication failed');
                                   break;
                              case Ext.form.Action.SERVER_INVALID:
                                   Ext.Msg.alert('Failure', action.result.msg);
                         }
                    }
                });
            },
            scope: this
        },
        {
            text: 'Cancel',
            scope: this,
            handler: function() {
                form1.hide();
                // userGrid.render('user-grid');
            }
        }]
    });

});
