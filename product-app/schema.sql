drop table if exists product;
drop table if exists producttype;
drop table if exists productum;
drop table if exists company;

-- a table that contains all the currently accounted for companies
create table company (
    id          integer primary key, -- database id
    sType       text,                -- company type: SA, SCA, SCS, SNC, SRL
    sName       text not null,       -- company name: straightforward
    sLocation   text not null,       -- location: complete address
    sCUI        integer              -- unique identification code (in RO)
);

-- a table that contains units of mesurement
create table productum (
    id          integer primary key,    -- database id
    sName       text not null,          -- the name of the unit (m, kg, etc.)
    sShort      text not null           -- short version of the name
);

create table producttype (
    id              integer primary key,    -- database id
    sName           text not null,          -- type name: food, sports, IT..
    sDescription    text                    -- description of the type
);

create table product (
    id                       integer primary key,
    sName                    text not null,
    nPrice                   real not null,
    nAmount                  integer not null,
    fType                    integer not null,
    fUM                      integer not null,
    fCompany                 integer not null,
    foreign key(fType)       references producttype(id),
    foreign key(fUM)         references productum(id),
    foreign key(fCompany)    references company(id)
);
