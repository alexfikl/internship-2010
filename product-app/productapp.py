# system imports
import re

# flask imports
from flask import Flask
from flask import request
from flask import g
from flask import render_template
from flask import jsonify

# my imports
from models import *
import utils

app = Flask(__name__)
app.config.from_object('config')

@app.before_request
def before_request():
    g.db = db

@app.teardown_request
def teardown_request(exception):
    g.db.session.close()

@app.route('/')
def show_root():
    return render_template('index.html')

@app.route('/app/', methods=['GET'])
def show_app():
    return render_template('index-app.html')

@app.route('/mobile/', methods=['GET'])
def show_mobile():
    return render_template('index-mobile.html')

@app.route('/api/product/', defaults={'pid': 0})
@app.route('/api/product/<int:pid>')
def get_product_info(pid):
    print("in get_product_info: {}".format(pid))

    if pid:
        pquery = Product.query.filter_by(id=pid).first()
        pquery = pquery.serialize() if pquery else {}
    else:
        pquery = [p.serialize() for p in Product.query.all()]

    return jsonify(utils.format_query(pquery))

@app.route('/api/company/', defaults={'cid': 0})
@app.route('/api/company/<int:cid>')
def get_company_info(cid):
    print("in get_company_info: {}".format(cid))
    if cid:
        pquery = Company.query.filter_by(id=cid).first()
        pquery = pquery.serialize() if pquery else {}
    else:
        pquery = [p.serialize() for p in Company.query.all()]

    return jsonify(utils.format_query(pquery))

@app.route('/product/edit/<int:pid>/<field>/<value>')
def test(pid, field, value):
    print(request.args.get('pid'))
    print(pid, field, value)

if __name__ == '__main__':
    app.run()
