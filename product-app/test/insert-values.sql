insert into company(sType, sName, sLocation, sCUI)
        values('SA', 'Google', 'USA', 42);
insert into company(sType, sName, sLocation, sCUI)
        values('SA', 'Facebook', 'USA', 41);
insert into company(sType, sName, sLocation, sCUI)
        values('SRL', 'Yahoo', 'USA', 40);
insert into company(sType, sName, sLocation, sCUI)
        values('SCA', 'British Tobacco', 'England', 43);
insert into company(sType, sName, sLocation, sCUI)
        values('SNC', 'Nestle', 'Germany', 44);
insert into company(sType, sName, sLocation, sCUI)
        values('SRL', 'Bananarama', 'Bangladesh', 45);

insert into productum(sName, sShort) values('Meters', 'm');
insert into productum(sName, sShort) values('Kilometers', 'km');
insert into productum(sName, sShort) values('Kilograms', 'kg');
insert into productum(sName, sShort) values('Litres', 'l');
insert into productum(sName, sShort) values('Pieces', 'piece');
insert into productum(sName, sShort) values('Grams', 'g');

insert into producttype(sName, sDescription)
        values('Food', 'Anything that can be eaten.');
insert into producttype(sName, sDescription)
        values('Sports', 'Like balls and bicycles and such.');
insert into producttype(sName, sDescription)
        values('Electronics', 'Anything that has a chip in it.');
insert into producttype(sName, sDescription)
        values('Apparel', 'Stuff you can wear outside.');
insert into producttype(sName, sDescription)
        values('Furniture', 'Stuff to put your stuff into.');

insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('Closet', 3.14, 1, 5, 5, 2);
insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('Banana', 0.14, 1000, 1, 3, 6);
insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('Water Bottle', 0.24, 2, 1, 4, 5);
insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('iPhone', 11.14, 1, 3, 5, 2);
insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('Dell Laptop', 12.14, 1, 3, 5, 1);
insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('Curry', 0.10, 5, 1, 6, 1);
insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('Scarf', 1.14, 1, 4, 5, 4);
insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('Tennit racket', 5.14, 2, 2, 5, 5);
insert into product(sName, nPrice, nAmount, fType, fUM, fCompany)
        values('Failure', 0.0, 9000, 3, 5, 3);
