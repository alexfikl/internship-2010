internship-2010
===============

A small web app I did for an internship thing: product database + frontend
to view/edit/add/delete.

WARNING: Not up to date.

Requires you to download ExtJs and Sencha Touch frameworks and add them to
media/js/.

Also require renaming the folders to 'ext', respectively 'sencha-touch',
and possibly renaming the project folder to 'champloo' because that's the
internal name I gave it.

And of course Django and Django-Piston.
